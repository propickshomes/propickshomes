
import './App.css';
import HomePage from './Pages/HomePage';
import ContactUs from './Pages/ContactUs';
import AboutUs from './Pages/AboutUs';
import NavigationBar from './Components/NavigationBar';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Register from './Pages/Register';
import Login from './Pages/Login';
import SellHouse from './Components/SellHouse';
import Services from './Pages/Services';
import Footer from './Components/Footer';
import BuyHouse from './Components/BuyHouse';
import RentHouse from './Components/RentHouse';
import ToletHouses from './Components/ToletHouses';



function App() {
  return (
    <div >
      
      <BrowserRouter>
      <NavigationBar />
      <Routes>
        <Route path='/' element={<HomePage />} />
        <Route path='/ContactUs' element={<ContactUs/>} />
        <Route path='/AboutUs' element={<AboutUs/>} />
        <Route path='/Register' element={<Register/>}/>
        <Route path='/Login' element={<Login/>}/>
        <Route path='/SellHouse' element={<SellHouse/>}/>
        <Route path='/Services' element={<Services/>}/>
        <Route path='/BuyHouse' element={<BuyHouse/>}/>
        <Route path='/RentHouse' element={<RentHouse/>}/>
        <Route path='/ToletHouses' element={<ToletHouses/>}/>

        

      </Routes>
      <Footer/>
      </BrowserRouter>
      
    </div>
  );
}

export default App;




