import React, { useEffect, useState } from 'react'
import { CardTitle, CardBody, CardText, Card } from 'reactstrap'
import axios from 'axios'
import rentalHouses from '../Images/rentalhouses.png'

function ToletHouses() {
    const mainContainer = {
       
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: '10px'

    }
    const cardStyles = {
        display:'flex',
        flexDirection:'row',
        flexWrap:'wrap',
        margin:'20px',
        textAlign:'center',
        borderRadius:'10px',

    }
    let [data, setData] = useState(null)
    useEffect(() => {
        const getData = async () => {
            try {
                let response = await axios.get("http://localhost:3005/getRentalHouses");
                console.log(response.data);
                setData(response.data)
            }
            catch (err) {
                console.log(err);
            }

        }
        getData();
    }, [])
    return (
        <div style={mainContainer}>
            <div style={cardStyles}>
                {data && data.map((item) => (
                    <Card
                        style={{
                            width: '15rem',
                            margin:'20px'
                        }}
                    >
                        <img
                            alt='house'
                            src={rentalHouses}
                        />
                        <CardBody>
                            <CardTitle tag="h5">
                                {item.email}
                            </CardTitle>
                            <CardText>
                                {item.number}
                            </CardText>
                            <CardText>
                                {item.address} ,  {item.city} , {item.state}.
                            </CardText>
                            <CardText>
                                Rent per Month: {item.money}
                            </CardText>
                        </CardBody>
                    </Card>

                ))}

            </div>
        </div>
      
        
    )
}

export default ToletHouses