import React, { useState,useEffect } from 'react';
// import { Input, FormGroup, Label, Col, Form, Row, Button, FormText } from 'reactstrap'
import axios from 'axios'
// import { useNavigate } from 'react-router-dom'

function Sellhouse() {
  // let navigate = useNavigate()
  // let [formdata, setFormdata] = useState({
  //   email: '',
  //   number: '',
  //   house: '',
  //   address: '',
  //   city: '',
  //   state: '',
  //   check: false
  // })
  // const [file,setFile] =useState()
  // // const [image,setImage] =useState()

  // const handleOnChage = (e) => {
  //   let { name, value, type, checked } = e.target;
  //   let inputValue = type === 'checkbox' ? checked : value
  //   setFormdata({
  //     ...formdata,
  //     [name]: inputValue
  //   })
  // }
  // const handlesubmit = (e) => {
  //   e.preventDefault();
  //   formdata.append('file',file)
  //   console.log(formdata)
  //   register();
  // }

  // let register = async () => {
  //   try {
  //     let response = await axios.post("http://localhost:3005/sellingHomes", formdata);
  //     if (response.data.ok === 1) {
  //       alert("Uploaded Successfully")
  //       setFormdata({
  //         email: '',
  //         number: '',
  //         house: '',
  //         address: '',
  //         city: '',
  //         state: '',
  //         file: '',
  //         check: false

  //       })
  //       navigate('/')

  //     }
  //   }
  //   catch (err) {
  //     console.log(err);
  //   }

  // }

  // return (
  //   <div style={{ border: '1px solid gray', width: '500px', padding: '1rem', margin: '3rem auto', borderRadius: '10px' }}>
  //     <Form onSubmit={handlesubmit}>
  //       <Row>
  //         <Col md={6}>
  //           <FormGroup>
  //             <Label for="exampleEmail">
  //               Email
  //             </Label>
  //             <Input
  //               id="exampleEmail"
  //               name="email"
  //               placeholder="name@gmail.com"
  //               type="email"
  //               value={formdata.email}
  //               onChange={handleOnChage}
  //               required
  //             />
  //           </FormGroup>
  //         </Col>
  //         <Col md={6}>
  //           <FormGroup>
  //             <Label for="exampleNumber">
  //               MobileNumber:
  //             </Label>
  //             <Input
  //               id="exampleNumber"
  //               name="number"
  //               placeholder="mobile number"
  //               type="number"

  //               value={formdata.number}
  //               onChange={handleOnChage}
  //               required
  //             />
  //           </FormGroup>
  //         </Col>
  //       </Row>
  //       <FormGroup row>
  //         <Label
  //           for="exampleSelect"
  //           sm={2}
  //         >
  //           Select
  //         </Label>
  //         <Col sm={10}>
  //           <Input
  //             id="exampleSelect"
  //             name="house"
  //             type="select"
  //             value={formdata.house}
  //             onChange={handleOnChage}
  //           >
  //             <option>
  //               select BHK
  //             </option>
  //             <option>
  //               1 BHK
  //             </option>
  //             <option>
  //               2 BHK
  //             </option>
  //             <option>
  //               3 BHK
  //             </option>
  //             <option>
  //               4 BHK
  //             </option>
  //             <option>
  //               Other
  //             </option>
  //           </Input>
  //         </Col>
  //       </FormGroup>
  //       <FormGroup>
  //         <Label for="exampleAddress">
  //           Address
  //         </Label>
  //         <Input
  //           id="exampleAddress"
  //           name="address"
  //           placeholder="Address"
  //           value={formdata.address}
  //           onChange={handleOnChage}
  //           required
  //         />
  //       </FormGroup>
  //       <Row>
  //         <Col md={6}>
  //           <FormGroup>
  //             <Label for="exampleCity">
  //               City
  //             </Label>
  //             <Input
  //               id="exampleCity"
  //               name="city"
  //               value={formdata.city}
  //               onChange={handleOnChage}
  //               required
  //             />
  //           </FormGroup>
  //         </Col>
  //         <Col md={4}>
  //           <FormGroup>
  //             <Label for="exampleState">
  //               State
  //             </Label>
  //             <Input
  //               id="exampleState"
  //               name="state"
  //               value={formdata.state}
  //               onChange={handleOnChage}
  //               required
  //             />
  //           </FormGroup>
  //         </Col>
  //       </Row>
  //       <FormGroup>
  //         <Label for="exampleFile">
  //           File
  //         </Label>
  //         <Input
  //           id="exampleFile"
  //           name="file"
  //           type="file"
  //           accept='image/*'
  //           value={formdata.file}
  //           onChange={e=>setFile(e.target.files[0])}
  //           required
  //         />
  //         <FormText>
  //           Choose image files only.
  //         </FormText>
  //       </FormGroup>

  //       <FormGroup check>
  //         <Input
  //           id="exampleCheck"
  //           name="check"
  //           type="checkbox"
  //           value={formdata.check}
  //           onChange={handleOnChage}
  //           required
  //         />
  //         <Label
  //           check
  //           for="exampleCheck"
  //         >
  //           Check me out
  //         </Label>
  //       </FormGroup>
  //       <Button type='submit'>
  //         Submit
  //       </Button>
  //     </Form>
  //   </div>
  // )

  const [file,setFile] =useState()
  const [image,setImage] =useState()
  const handleUpload =(e) => {
    const formdata =new FormData()
    formdata.append('file',file)

      axios.post('http://localhost:3003/upload',formdata)
      .then(res=>console.log(res))
      .catch(err=>console.log(err))
  }
  
  useEffect(()=>{
    axios.get('http://localhost:3003/getImage')
    .then(res=>setImage(res.data[7].image))
    .catch(err=>console.log(err))
  },[])
return (
  <div>
      <input type="file" onChange={e=>setFile(e.target.files[5])}/>
      <button onClick={handleUpload}>Upload</button>
      <br/>
      <img src={'http://localhost:3003/Images/'+image} alt='' style={{height:'300px',width:'300px'}}/>
  </div>
  
)
}

export default Sellhouse