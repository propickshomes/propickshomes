import React, { useEffect, useState } from 'react'
import { CardTitle, CardBody, CardText, Card } from 'reactstrap'
import axios from 'axios'
import houseImage from '../Images/buyhouses.png'

function BuyHouse() {
    const mainContainer = {
       
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        padding: '10px'

    }
    const cardStyles = {
        display:'flex',
        flexDirection:'row',
        flexWrap:'wrap',
        margin:'20px',
        textAlign:'center',
        borderRadius:'10px',
        
    }
    let [data, setData] = useState(null)
    useEffect(() => {
        const fetchData = async () => {
            try {
                let response = await axios.get("http://localhost:3005/gethouses");
                console.log(response.data);
                setData(response.data)
            }
            catch (err) {
                console.log(err);
            }

        }
        fetchData();
    }, [])
    return (
        <div style={mainContainer}>
            <div style={cardStyles}>
                {data && data.map((item) => (
                    <Card
                        style={{
                            width: '15rem',
                            margin:'20px'
                        }}
                    >
                        <img
                            alt='house'
                            src={houseImage}
                        />
                        <CardBody>
                            <CardTitle tag="h5">
                                {item.email}
                            </CardTitle>
                            <CardText>
                                {item.number}
                            </CardText>
                            <CardText>
                                {item.address} ,  {item.city} , {item.state}.
                            </CardText>
                        </CardBody>
                    </Card>

                ))}

            </div>
        </div>
        //     <div style={{display:'flex',flexWrap:'wrap',justifyContent:'space-around',width:500,height:5}}>
        //         <CardGroup>
        //   <Card >
        //     <CardImg 
        //       alt="Card image cap"
        //       src="https://picsum.photos/318/180"
        //       top
        //       width="100%"
        //     />
        //     <CardBody >
        //       <CardTitle tag="h5">
        //         Card title
        //       </CardTitle>
        //       <CardSubtitle
        //         className="mb-2 text-muted"
        //         tag="h6"
        //       >
        //         Card subtitle
        //       </CardSubtitle>
        //       <CardText>
        //         This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.
        //       </CardText>
        //       <Button>
        //         Button
        //       </Button>
        //     </CardBody>
        //   </Card>
        //   <Card>
        //     <CardImg
        //       alt="Card image cap"
        //       src="https://picsum.photos/318/180"
        //       top
        //       width="100%"
        //     />
        //     <CardBody>
        //       <CardTitle tag="h5">
        //         Card title
        //       </CardTitle>
        //       <CardSubtitle
        //         className="mb-2 text-muted"
        //         tag="h6"
        //       >
        //         Card subtitle
        //       </CardSubtitle>
        //       <CardText>
        //         This card has supporting text below as a natural lead-in to additional content.
        //       </CardText>
        //       <Button>
        //         Button
        //       </Button>
        //     </CardBody>
        //   </Card>
        //   <Card>
        //     <CardImg
        //       alt="Card image cap"
        //       src="https://picsum.photos/318/180"
        //       top
        //       width="100%"
        //     />
        //     <CardBody>
        //       <CardTitle tag="h5">
        //         Card title
        //       </CardTitle>
        //       <CardSubtitle
        //         className="mb-2 text-muted"
        //         tag="h6"
        //       >
        //         Card subtitle
        //       </CardSubtitle>
        //       <CardText>
        //         This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.
        //       </CardText>
        //       <Button>
        //         Button
        //       </Button>
        //     </CardBody>
        //   </Card>
        // </CardGroup>
        //     </div>
    )
}

export default BuyHouse