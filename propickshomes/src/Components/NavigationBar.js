import React from 'react'
import { Link } from 'react-router-dom'
import Image from '../Images/logo.png'



function Navbar() {
  return (
    <div >
      <nav className="navbar navbar-expand-lg navbar-light">
        <img src={Image} alt='' height={60} width={200}></img>
        <h1 style={{ color: 'darkblue' }}>ProPicks Homes</h1>

        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>


        <div className="collapse navbar-collapse" id="navbarSupportedContent">

          <ul className="navbar-nav mr-auto">
            <li className="nav-item active p-2 ">
              <Link to="/" className="nav-link text-orange" href="#"><h4 style={{ color: 'crimson' }}>HomePage</h4></Link>
            </li>
            <li className="nav-item active p-2 ">
              <Link to="/AboutUs" className="nav-link text-orange" href="#"><h4 style={{ color: 'crimson' }}>AboutUs</h4></Link>
            </li>
            <li className="nav-item active p-2">
              <Link to="/ContactUs" className="nav-link text-orange" href="#"><h4 style={{ color: 'crimson' }}>ContactUs</h4></Link>
            </li>
            <li className="nav-item active p-2 ">
              <Link to="/Register" className="nav-link text-orange" href="#"><h4 style={{ color: 'crimson' }}>Register</h4></Link>
            </li>
            <li className="nav-item active p-2">
              <Link to="/Login" className="nav-link text-orange" href="#"><h4 style={{ color: 'crimson' }}>Login</h4></Link>
            </li>
          </ul>
        </div>
      </nav>

    </div>
  )
}

export default Navbar