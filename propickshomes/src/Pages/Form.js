import React from 'react'

function Form() {
  return (
    <div style={{border:'1px solid gray',width:'500px',padding:'1rem',margin:'3rem auto',borderRadius:'10px'}}><form>
    <div class="form-group">
      <label for="exampleFormControlInput1">Email address</label>
      <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com"/>
    </div>
    <div class="form-group">
    <label for="exampleFormControlFile1">Phone Number</label>
    <input type="number" id="replyNumber" min="0" step="1" data-bind="value:replyNumber" />
  </div>
    <div class="form-group">
      <label for="exampleFormControlSelect1"> select number of BHK</label>
      <select class="form-control" id="exampleFormControlSelect1">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>other</option>

      </select>
    </div>
    <div class="form-group">
      <label for="exampleFormControlTextarea1"> If other,enter the number</label>
      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
   
    <div class="form-group">
      <label for="exampleFormControlTextarea1"> Address of the site</label>
      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
    </div>
    <div class="form-group">
    <label for="exampleFormControlFile1">Images of Property</label>
    <input type="file" name="myImage" accept="image/*" />
  </div>
  <button class="btn btn-primary" type="submit">Submit form</button>
  </form>
  </div>
  )
}

export default Form