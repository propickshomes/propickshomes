import React from 'react'
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
// import axios from 'axios';
// import { Card, CardBody, CardTitle, CardSubtitle, CardText } from 'reactstrap';

import carousel1 from '../Images/carousel1.png'
import carousel2 from '../Images/carousel2.png'
import carousel3 from '../Images/carousel3.png'
import Services from './Services';


function HomePage() {
  const carouselStyles = {
    height: '400px',
    margin: ' 0px'
  }
  // const cardStyles = {
  //   margin: '0px 0px',
  //   display: 'flex',
  //   justifyContent: 'space-between'
  // }
  // let [data, setData] = useState(null)
  // useEffect(() => {
  //   const fetchData = async () => {
  //     try {
  //       let response = await axios.get("http://localhost:3005/getdetails");
  //       console.log(response.data);
  //       setData(response.data)
  //     }
  //     catch (err) {
  //       console.log(err);
  //     }

  //   }
  //   fetchData();
  // }, [])
  return (
    <div>


      <div >
<div style={{border:'5px solid gray'}}>
        <marquee><h2 style={{ color: 'orange' }}>Thank you for considering ProPicks Homes. We look forward to serve you!</h2></marquee>
        </div>
        <Carousel autoPlay interval="5000" transitionTime="5000" showThumbs={false} infiniteLoop={true}>
          <div style={carouselStyles}>
            <img src={carousel1} alt="" style={{ height: '400px' }} />
          </div>
          <div style={carouselStyles}>
            <img src={carousel2} alt='' style={{ height: '400px' }} />
          </div>
          <div style={carouselStyles}>
            <img src={carousel3} alt='' style={{ height: '400px' }} />
          </div>
        </Carousel>
      </div>

      {/* <div style={cardStyles}>
        {data && data.map((item) => (
          <Card
            style={{
              width: '16rem'
            }}
          >
            <CardBody>
              <CardTitle tag="h5">
                <h1>{item.productname}</h1>
              </CardTitle>
              <CardSubtitle
                className="mb-2 text-muted"
                tag="h6"
              >
                {item.id}

              </CardSubtitle>
            </CardBody>
            <img
              alt="Card cap"
              src="https://picsum.photos/318/180"
              width="100%"
            />
            <CardBody>
              <CardText>
                {item.description}
              </CardText>
              <CardText>
                <h4>Price {item.price}</h4>
              </CardText>
              <CardText >
                <h4>discount {item.discount}</h4>
              </CardText>
            </CardBody>
          </Card>
        ))}
      </div> */}
      <Services />
    </div>


  )
}

export default HomePage
