import React, { useState } from 'react'
import { Input, FormGroup, Label, Col, Form, Row, Button } from 'reactstrap'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import bgimg from '../Images/bgimg.png.avif'


function Register() {
  let navigate = useNavigate()
  let [formdata, setFormdata] = useState({
    email: '',
    password: '',
    address: '',
    city: '',
    state: '',
    check: false

  })
  const handleOnChage = (e) => {
    let { name, value, type, checked } = e.target;
    let inputValue = type === 'checkbox' ? checked : value
    setFormdata({
      ...formdata,
      [name]: inputValue
    })
  }
  const handlesubmit = (e) => {
    e.preventDefault();
    console.log(formdata)
    register();
  }

  let register = async () => {
    try {
      let response = await axios.post("http://localhost:3005/registerUser", formdata);
      if (response.data.ok === 1) {
        alert("Registration Successful")
        setFormdata({
          email: '',
          password: '',
          address: '',
          city: '',
          state: '',
          check: false

        })
        navigate('/Login')
      }
    }
    catch (err) {
      console.log(err);
    }
  }

  return (
    <div style={{ border: '1px solid gray',  padding: '1rem', margin: '3rem 25rem',borderRadius:'10px',backgroundImage:`url(${bgimg})`}}>
        <Form onSubmit={handlesubmit}>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleEmail">
                  Email
                </Label>
                <Input
                  id="exampleEmail"
                  name="email"
                  placeholder="Email"
                  type="email"
                  value={formdata.email}
                  onChange={handleOnChage}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="examplePassword">
                  Password:
                </Label>
                <Input
                  id="examplePassword"
                  name="password"
                  placeholder="password"
                  type="password"
                  pattern='(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}'
                  title='Must contain at least one  number and one uppercase and lowercase letter, and at least 6 or more characters'
                  value={formdata.password}
                  onChange={handleOnChage}
                  required
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Label for="exampleAddress">
              Address
            </Label>
            <Input
              id="exampleAddress"
              name="address"
              placeholder="Address"
              value={formdata.address}
              onChange={handleOnChage}
              required
            />
          </FormGroup>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleCity">
                  City
                </Label>
                <Input
                  id="exampleCity"
                  name="city"
                  value={formdata.city}
                  onChange={handleOnChage}
                  required
                />
              </FormGroup>
            </Col>
            <Col md={4}>
              <FormGroup>
                <Label for="exampleState">
                  State
                </Label>
                <Input
                  id="exampleState"
                  name="state"
                  value={formdata.state}
                  onChange={handleOnChage}
                  required
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup check>
            <Input
              id="exampleCheck"
              name="check"
              type="checkbox"
              value={formdata.check}
              onChange={handleOnChage}
              required
            />
            <Label
              check
              for="exampleCheck"
            >
              Check me out
            </Label>
          </FormGroup>
          <Button type='submit'>
            Register
          </Button>
        </Form>
    </div>
  )
}

export default Register