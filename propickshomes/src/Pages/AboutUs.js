import React from 'react'
import logo from '../Images/logo.png'
import contact from '../Images/contactimage.png'
import aboutus from '../Images/aboutus.png'
import chooseus from '../Images/chooseus.png'
import wecando from '../Images/wecando.png'

function AboutUs() {
  return (
    <div>
      <div style={{ margin:'50px',border:'5px solid gray',padding:'40px'}}>
      <img src={logo} alt="" style={{height:'120px',width:'40%', float:'right'}}/>
      
      <h1>Welcome to ProPicks Homes</h1>
      <p>At ProPicks Homes, we're dedicated to simplifying the process of buying and selling homes. Our mission is to provide a seamless experience for our clients, whether they're searching for their dream home or looking to sell their property quickly and efficiently.</p>
      </div>
      {/* <div style={{margin:'30px'}}>
      <h2>Who We Are</h2>
      <p>ProPicks Homes is a team of passionate real estate professionals with years of experience in the industry. We understand that buying or selling a home can be one of the most significant decisions in your life, and we're here to guide you through every step of the process. Our expertise, combined with our commitment to personalized service, ensures that our clients receive the highest level of support and assistance.</p>
      </div> */}
      <div style={{margin:'50px',border:'5px solid gray',padding:'40px'}}>
      <img src={wecando} alt="" style={{height:'180px',width:'40%', float:'left'}}/>
      <h2>What We Do</h2>
      <p>As a full-service real estate agency, we offer a range of services to meet the diverse needs of our clients. Whether you're a first-time homebuyer, an experienced investor, or a homeowner looking to sell, we're here to help. From conducting market research and negotiating contracts to coordinating inspections and facilitating closings, we handle all aspects of the buying and selling process with professionalism and care.</p>
      </div>
      <div style={{margin:'50px',border:'5px solid gray',padding:'40px'}}>
      <img src={aboutus} alt="" style={{height:'120px',width:'40%', float:'left'}}/>
      <h2>Our Values</h2>
      <p>At ProPicks Homes, integrity, transparency, and client satisfaction are at the core of everything we do. We believe in building lasting relationships with our clients based on trust, honesty, and mutual respect. We're committed to providing expert advice, clear communication, and unparalleled customer service to ensure that your real estate experience is positive and rewarding.</p>
      </div>
      <div style={{margin:'50px',border:'5px solid gray',padding:'40px'}}>
      <img src={chooseus} alt="" style={{height:'120px',width:'40%', float:'left'}}/> 
      <h2>Why Choose Us</h2>
      <p>When you choose ProPicks Homes, you're choosing a partner who is dedicated to your success. We leverage our knowledge, resources, and network to help you achieve your real estate goals efficiently and effectively. Whether you're buying or selling, you can count on us to be your advocate every step of the way.</p>
      </div>
      <div style={{margin:'50px',border:'5px solid gray',padding:'40px'}}>
      <img src={contact} alt="" style={{height:'150px',width:'40%', float:'left', margin:'20px'}}/>
      <h2>Get in Touch</h2>
      <p>Ready to take the next step in your real estate journey? We're here to help! Contact us today to learn more about our services or to schedule a consultation with one of our experienced agents. Let us show you why ProPicks Homes is the right choice for all your real estate needs.</p>
      
      </div>
      {/* <marquee style={{margin:'30px'}}><h2>Thank you for considering ProPicks Homes. We look forward to serve you!</h2></marquee> */}
      
    </div>
  )
}

export default AboutUs