import React from 'react'
import contactusimage from '../Images/contactus.png'
import email from '../Images/emaillogo.png'
import call from '../Images/calllogo.png'
import address from '../Images/addresslogo.png'

function ContactUs() {
  return (
    <div>      
       <img src={contactusimage} alt="" style={{height:'330px',width:'100%',margin:'0px 0px 20px 0px'}}/>
       <div>
        <h2 style={{margin:'10px'}}>Let's connect</h2>
        <div style={{margin:'20px'}}>
        <img src={email} alt='' /> propickshomes@gmail.com
        </div>
        <div style={{margin:'20px'}}>
        <img src={call} alt=''/> 9876543210
       </div>
       <div style={{margin:'20px'}}>
        <img src={address} alt=''/> TalentSprint, Gachibowli, Hyderabad.
       </div>
       </div>
    </div>
  )
}

export default ContactUs