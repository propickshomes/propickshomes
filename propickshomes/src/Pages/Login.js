import React, { useState } from 'react'
import axios from 'axios';
import { Input, FormGroup, Label, Col, Form, Button } from 'reactstrap'
import { useNavigate } from 'react-router-dom';
import bgimg2 from '../Images/bgimg2.avif'

function Login() {
  let navigate = useNavigate()
  let [formdata, setFormdata] = useState({
    email: '',
    password: '',
    check: false,

  })
  const handleOnChage = (e) => {
    let { name, value, type, checked } = e.target;
    let inputValue = type === 'checkbox' ? checked : value
    setFormdata({
      ...formdata,
      [name]: inputValue
    })
  }
  const handlesubmit = (e) => {
    e.preventDefault();
    console.log(formdata)
    login();
  }

  let login = async () => {
    try {
      let response = await axios.post("http://localhost:3005/checkingUser", formdata);
      if (response.data.ok === 1) {
        alert("Login Successful")
        setFormdata({
          email: '',
          password: '',
          check: false,

        })
        navigate('/ContactUs')
      }
    }
    catch (err) {
      console.log(err);
    }
  }

  return (
    
    <div style={{ border: '1px solid gray', padding: '1rem', margin: '3rem 25rem', borderRadius:'10px',backgroundImage:`url(${bgimg2})`}}>
      <Form onSubmit={handlesubmit}>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleEmail">
              Email
            </Label>
            <Input
              id="exampleEmail"
              name="email"
              placeholder="Email"
              type="email"
              value={formdata.email}
              onChange={handleOnChage}
              required
            />
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label for="examplePassword">
              Password
            </Label>
            <Input
              id="examplePassword"
              name="password"
              placeholder="password"
              type="password"
              value={formdata.password}
              onChange={handleOnChage}
              required
            />
          </FormGroup>
        </Col>
        <FormGroup check>
          <Input
            id="exampleCheck"
            name="check"
            type="checkbox"
            value={formdata.check}
            onChange={handleOnChage}
            required
          />
          <Label
            check
            for="exampleCheck"
          >
            Check me out
          </Label>
        </FormGroup>
        <Button type='submit'>
          Login
        </Button>
      </Form>
    </div>
  )
}

export default Login