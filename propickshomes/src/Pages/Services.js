import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import backgroundimage from '../Images/bgimg_services.png'
import buyhouseimage from '../Images/buyhouse.png'
import sellhouseimage from '../Images/sellhouse.png'
import rentalhouse from '../Images/rentalhouse.png'
import tolethouse from '../Images/tolet.png'


function Services() {
  let navigate = useNavigate()
  const mainContainer={
    backgroundImage: `url(${backgroundimage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize:"cover",       
    
    display:'flex',
    flexWrap:'wrap',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-evenly',
    padding:'10px'
  }
  const containerStyles={
    width:'250px',
    margin:'20px',
    textAlign:'center',
    borderRadius:'10px'
  }
  const imgStyles={
    height:'200px',
    width:'250px',
    borderRadius:'10px'
  }

  const [isHovering, setIsHovering] = useState(false);

  const handleMouseEnter = () => {
    setIsHovering(true);
  };

  const handleMouseLeave = () => {
    setIsHovering(false);
  };

  const [isHover, setIsHover] = useState(false);

  const handleHoverEnter = () => {
    setIsHover(true);
  };

  const handleHoverLeave = () => {
    setIsHover(false);
  };

  const [isHoveringButton, setIsHoveringButton] = useState(false);

  const handleButtonEnter = () => {
    setIsHoveringButton(true);
  };

  const handleButtonLeave = () => {
    setIsHoveringButton(false);
  };
 
  const [isHoverButton, setIsHoverButton] = useState(false);

  const handleHoverButtonEnter = () => {
    setIsHoverButton(true);
  };

  const handleHoverButtonLeave = () => {
    setIsHoverButton(false);
  };
 

  const handleSellHouse = (e) => {
    e.preventDefault();
    navigate('/SellHouse')
    
  }
  const handleBuyHouse = (e) => {
    e.preventDefault();
    navigate('/BuyHouse')
  }
  const handleRentalHouse=(e)=>{
    e.preventDefault();
    navigate('/RentHouse')
  }
  const handleToletHouse=(e)=>{
    e.preventDefault();
    navigate('/ToletHouses')
  }
  return (

    <div style={mainContainer}>

       <div style={containerStyles}>
        <img src={buyhouseimage} alt='' style={imgStyles}/>
        <button onClick={handleBuyHouse} style={{ borderRadius:'5px',margin:'10px',background:'black',backgroundColor: isHover ? 'gray' : 'white',
            color: isHover ? 'white' : 'black',height: isHover ? '60px':'50px'}} onMouseEnter={handleHoverEnter}
            onMouseLeave={handleHoverLeave}>BUY HOUSE</button>
       </div>
       <div style={containerStyles}>
        <img src={sellhouseimage} alt='' style={imgStyles}/>
        <button onClick={handleSellHouse} style={{ borderRadius:'5px',margin:'10px',background:'black',backgroundColor: isHoveringButton ? 'gray' : 'white',
            color: isHoveringButton ? 'white' : 'black',height: isHoveringButton ? '60px':'50px'}} onMouseEnter={handleButtonEnter}
            onMouseLeave={handleButtonLeave}>SELL HOUSE</button>
       </div>
       <div style={containerStyles}>
        <img src={rentalhouse} alt='' style={imgStyles}/>
        <button onClick={handleRentalHouse} style={{ borderRadius:'5px',margin:'10px',background:'black',backgroundColor: isHoverButton ? 'gray' : 'white',
            color: isHoverButton ? 'white' : 'black',height: isHoverButton ? '60px':'50px'}} onMouseEnter={handleHoverButtonEnter}
            onMouseLeave={handleHoverButtonLeave}>HOUSE FOR RENT</button>
       </div>
       <div style={containerStyles}>
        <img src={tolethouse} alt='' style={imgStyles}/>
        <button onClick={handleToletHouse} style={{ borderRadius:'5px',margin:'10px',background:'black',backgroundColor: isHovering ? 'gray' : 'white',
            color: isHovering ? 'white' : 'black',height: isHovering ? '60px':'50px'}} onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}>HOUSE FOR RENT</button>
       </div>
    </div>
  )
}

export default Services