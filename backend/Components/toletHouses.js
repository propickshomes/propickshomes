var express = require("express")

var mongodb = require("mongodb")
var client = mongodb.MongoClient;

let fetch = express.Router().get('/', (req, res) => {
    client.connect("mongodb://localhost:27017/houses", (err, db) => {
        if (err) {
            throw err;
        }
        else {
            db.collection("rentalhouses").find({}).toArray((err, result) => {
                if (err) {
                    throw err
                }
                else {
                    res.send(result)
                }
            })

        }
    })

})

module.exports = fetch